package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
	"context"
	"flag"
	ini "github.com/go-ini/ini"
	"github.com/kardianos/service"
	"path"
)

var (
	logger   service.Logger
	configPath string
	config Config
)
type Config struct{
	Name, DisplayName, Description string
	http_url string
	proxy string
	interval int// interval timer (second)
}

type program struct {
	exit chan struct{}
}
func handler(w http.ResponseWriter, r *http.Request) {
	d := time.Now().Format("Mon Jan 2 15:04:05 2006 MST")
	fmt.Fprintln(w, d)
}


func (p *program) Start(s service.Service) error {
	date, err := getDate()
	if err != nil {
		logger.Infof("getDate():%v", err)
	}
	err = setDate(date)
	if err != nil {
		logger.Infof("setDate():%v", err)
	}
	l, err := net.Listen("tcp4", ":8123")
	if err != nil {
		logger.Infof("net.Listen():%v", err)
	}
	server := &http.Server{Handler: http.HandlerFunc(handler)}
	go func(){
		 server.Serve(l)
	}()

	go func(){
		<-p.exit
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		server.Shutdown(ctx)
	}()

	go p.run()
	return nil
}
func (p *program) run() error {
	logger.Infof("I'm running %v.", service.Platform())
	ticker := time.NewTicker(time.Duration(config.interval) * time.Second)
	for {
		select {
		case <-ticker.C:
			date, err := getDate()
			if err != nil {
				logger.Errorf("getDate():%v", err)
				continue
			}
			err = setDate(date)
			if err != nil {
				logger.Errorf("setDate():%v", err)
				continue
			}
			logger.Infof("set date %v...", date)
		case <-p.exit:
			ticker.Stop()
			return nil
		}
	}
}
func (p *program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	logger.Info("I'm Stopping!")
	close(p.exit)
	return nil
}

func parseOpt()(f *flag.FlagSet, err error){
	args := os.Args
	f = flag.NewFlagSet(args[0], flag.ContinueOnError)
	f.StringVar(&configPath, "f", "", "config file path")
	f.Usage=func(){
		fmt.Fprintf(os.Stderr, `Usage: %s [-f config path] "install" or "start" or "uninstall"
`, path.Base(os.Args[0]));
		f.PrintDefaults()
	}
	err = f.Parse(args[1:])
	if err != nil {
		return
	}
	return
}
func getConfig() (err error){
	var cfg *ini.File
	if configPath == "" {
		if runtime.GOOS == "windows" {
			configPath = `c:\datesync\datesync.conf`
		} else {
			configPath = "/etc/datesync.conf"
		}
	}
	cfg, err = ini.InsensitiveLoad(configPath)
	if err != nil{
		log.Println("ini file not loaded :", err)
	}else{
		config.http_url = cfg.Section("").Key("url").String()
		config.proxy = cfg.Section("").Key("proxy").String()
		config.interval = cfg.Section("").Key("interval").MustInt()
	}
	if len(config.http_url) == 0 {
		config.http_url = "http://ntp-a1.nict.go.jp/cgi-bin/time"
	}
	if config.interval == 0 {
		config.interval = 3600
	}
	return
}
func getDate() (date string, err error) {
	// Start should not block. Do the actual work async.
	var resp *http.Response
	if len(config.proxy) > 0 {
		proxyUrl, err := url.Parse(config.proxy)
		if err != nil {
			log.Printf("proxy url parse error, url=%s:%v", proxyUrl, err)
			return "", err
		}
		client := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}
		resp, err = client.Get(config.http_url)
		if err != nil {
			log.Printf("http get error, http_url=%s:%v", config.http_url, err)
			return "", err
		}
	}else{
		client := &http.Client{Transport: &http.Transport{}}
		resp, err = client.Get(config.http_url)
		if err != nil {
			log.Printf("http get error, http_url=%s:%v", config.http_url, err)
			return "", err
		}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("response body error:", err)
		return
	}
	date = strings.TrimSpace(string(body))
	return
}
func setDate(date string) (err error) {
	if runtime.GOOS == "windows" {
		timeformat := "Mon Jan 2 15:04:05 2006 MST"
		t, err := time.Parse(timeformat, date)
		if err != nil {
			return err
		}
		d := fmt.Sprintf("%d-%d-%d", t.Year(), t.Month(), t.Day())
		err = exec.Command("cmd", "/c", "date", d).Run()
		if err != nil {
			return err
		}
		s := fmt.Sprintf("%d:%d:%d", t.Hour(), t.Minute(), t.Second())
		err = exec.Command("cmd", "/c", "time", s).Run()
		if err != nil {
			return err
		}
	} else {
		err = exec.Command("/usr/bin/date", "-s", date).Run()
		if err != nil {
			return err
		}
	}
	return
}
func main() {
	f, err := parseOpt()
	if err != nil {
		os.Exit(1)
	}
	svcConfig := &service.Config{
		Name:        "datesync",
		DisplayName: "date synchronize service",
		Description: "This is program that synchronize date.",
	}
	prg := &program{exit:make(chan struct{})}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Fatal(err)
	}
	if f.Arg(0) == "install" {
		 getConfig()
		_, err = getDate()
		if err != nil {
			log.Fatalf("getDate():%v", err)
		}
	}
	err = service.Control(s, f.Arg(0))
	if err != nil {
		log.Fatal(err)
	}
	logger, err = s.Logger(nil)
	if err != nil {
		log.Fatal(err)
	}
	err = s.Run()
	if err != nil {
		logger.Error(err)
	}
}
