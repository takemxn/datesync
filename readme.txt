・インストール方法

Linux：
  ・datesync.confを修正して、/etc/datesync.confへ配置
  ・下記を実施（サービスインストール・起動）
    $ sudo ./datesync install
    $ sudo ./datesync start

Windows：
  管理者で実施
  ・datesync.confを修正して、c:\datesync\datesync.confへ配置
  ・下記を実施（サービスインストール・起動）
    > datesync.exe install
    > datesync.exe start
